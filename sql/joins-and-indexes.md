## Joins

- allows us to create a single query which spans across multiple tables in our db
- there are various different types of joins, based on what information you would like from each table, and how you define the tables to be joined together
- the typical join syntax is a follows:
- inner, left (outer), right (outer), full (outer)

```sql
select [columns]
from [left table]
left/right/full join [right table]
on [join predicate];
```

There are various different types of joins, based on what information you would like from each table, and how you define a join predicate. A *theta* join is just a join which joins two tables based on some condition (defined above as the join predicate). An *equi* join, is a theta join, where that condition uses equality. 

```sql 
select employee.name, department.name
from employee
left join department
on department.id = employee.department;
```

We also have such joins as *natural* joins. These joins are implicit and do not need a join predicate. Instead, the join is performed based on columns with the same names. In the employee/department example above, if the department table had an id column with the name 'dept_id' and the employee table had a column with the name 'dept_id' which referred to the department table, a natural join could be performed.

```sql 
select employee.name, department.name
from employee
natural join department
```

## Index
An index is a common way to enhance database performance, enabling faster retrieval. When creating an index, the database stores an in memory ordering of a particular column. Without an index, if we were searching on a particular column, we would need to check every single value in that column to return the result. Indexes allow those operations to be more efficient. Columns which are searched on frequently, and that have a high percent of unique values and a low percent of null values are generally good candidates for an index. While indexes can make searching operations more efficient, we need to be careful not to use indexes too liberally. Because we are storing an additional ordering in memory, they need to be maintained every time we perform operations like insertions and deletions, so they may not always be the best choice.
