# HTML / Git Exercise 

- please send me your GitLab username if you have not already (this is not necessarily the same as your GitHub username, you can use GitHub to authenticate, but you will need to log in and authorize GitLab before GitLab will generate your username)

---

- work in groups of 3 or 4 to create a web page or two to represent our task app, one person in each group should share their screen so the team can work on one machine
- first, clone the [html-exercise](https://gitlab.com/210907-java-ms-ext/training-repositories/html-exercise) repo in our GitLab group
    > git clone https://gitlab.com/210907-java-ms-ext/training-repositories/html-exercise.git
- next, make a branch for your group
    > git checkout -b group-#
- work with your group to put together either (1) one single page that displays task data and a form for creating a task or (2) two separate pages, one which displays task data and another which includes a form to create a new task
    - the purpose of this exercise is to get comfortable with HTML, your web page(s) do not need to be functional, task data can be hardcoded
- optional: add a little css styling to make your web page pretty :)
- when you complete your web page(s), commit your changes to your team's branch on your local repository
    - add changes to the staging area
    > git add .
    - commit staged changes 
    > git commit -m "your commit message"
- then push your branch up to our GitLab repository 
    > git push -u origin [your branch name]
    
---

- I would recommend working with Visual Studio Code for this - to open your html file in your browser, you can right click on the file tab, copy the path, and then paste the path in your browser. This will render your html file in your browser.
