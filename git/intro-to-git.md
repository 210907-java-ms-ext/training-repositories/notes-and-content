# Some helpful git resources:

[Git cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet/)

[Tool which allows you to visualize git commands](https://learngitbranching.js.org/?locale=en_US)

- take a look at the "Remote" goals in the first modal

# Creating a Repository

Within your subgroup, you have owner permissions, and have the ability to create and manage your own repositories. There are two approaches to creating a local repository which is configured with a remote repository on GitLab.

1. Gitlab Project First:

   > git clone [repo url].git

   > cd test

   > touch README.md

   > git add README.md

   > git commit -m "add README"

   > git push -u origin master

2. Local repository first

   > cd existing_folder

   > git init

   > git remote add origin [repo url].git

   > git add .

   > git commit -m "Initial commit"

   > git push -u origin master

# Pushing additional changes to your remote repository.

> git add [. -A or a particular file/directory]

> git commit -m "commit message"

> git push


# Pulling Notes/Code from the training content repo

Clone the remote repository to create a copy of it locally:

> git clone https://gitlab.com/210907-java-ms-ext/training-repositories/notes-and-content.git

Navigating into the "training-content" folder, will give you the contents of the repository.

If you need to update the training-content repository, you can use `git pull` to pull any additional commits.
