# JavaScript

JavaScript was created by Brendan Eich in 1995 during his time at Netscape. Since then, it has become the predominant programming language for the web. At a surface level, facts about JavaScript can seem awkward and unnecessarily confusing, where seemingly simple questions like "who owns JavaScript?", "is JavaScript really an Object-Orient Programming language?", or "is JavaScript compiled or interpreted?" have answers that are surprisingly nuanced.

At first, JavaScript was used primarily for developing client-side components of applications, where the web browser provides the runtime environment which executes the code. It was also traditionally considered an interpreted language as you give browsers JavaScript, and then browsers will know how to interpret these instructions by default.

When a browser runs JS, it uses some sort of engine to actually execute the script. Each browser has it's own engine. This initially caused a lot of capability issues, when the same code was run in different browsers. The need for a standardization of the language led to the ECMAScript Language Specification. While there are still some issues with browser compatibility and browsers supporting the latest ECMAScript version, the specification has done a lot to make JavaScript more consistent.

Most modern day browsers, do actually perform JIT (Just-In-Time) compilation, to translate JavaScript code into more efficient machine code, instead of using an interpreter. This is done in teh V8 engine, which was developed by Google and is used to power Google Chrome and provides the runtime for node.

- implementation of ECMAScript, a scripting-language specification
  - [ES6 (ES2015)](http://es6-features.org/#Constants)
    - some ES6 features
    - let, const
    - arrow notation (similar to lambdas in Java)
    - for/of loop
    - symbol
    - template literals
  - ECMAScript 2016, 2017, 2018
  - European Computer Manufacturers Association
- a language we can use in conjunction with our HTML files to add functionality and enhance the capabilities of our webpages
- JavaScript was traditionally interpreted by the browser (most recent browsers have JIT compilers for optimization)
  - without JIT compilation, each time a loop executes it would translate the inner code to machine code
  - with JIT compilation, the inner code would be translated to machine code once and then the machine code would be executed for every iteration of the loop
- flexible language that can utilize both object oriented and functional elements
- loosely typed
- single threaded

# Scope

- local scope (lexical scope)
- global scope
- block scope (ES6 +)

We don't have access modifiers in JavaScript, but we can use this scope to restrict access to variables. A closure, gives you access to an outer function's scope from an inner function. 

``` javascript
function User() {

    let id = 5;
  
    this.setId = function(_id){
        id = _id;
    }
    
    this.getId = function(){
        return id;
    }

}
```

We create a constructor function for a User object, giving the user object two properties: `setId` and `getId`. An `id` variable is declared, but as it's defined in the lexical scope of the User function, it is not accessible outside of the function. We can access `id`, but only indirectly through the get/set function.

# Declaring a variable

- keywords to declare variable:
  - var
    - declares a variable in the current scope (local or global)
    - variables are able to be "hoisted" to the top of the scope (they can be used before they are declared)
  - let
    - declares a variable in the current scope (global, local, or block)
    - cannot be redeclared
  - const
    - declares a variable in the current scope (global, local, or block)
    - cannot be declared without being assigned
    - cannot be redeclared or reassigned

## Data types

- number (2^64, 64 bit floating point)
- string
- boolean
- null
- undefined
- symbol

Everything else is an object

- object is a combination of key value pairs

```javascript
let cat = {
  name: "Ferdinand",
  breed: "calico",
  age: 2,
};
```
## Arrays 

The JavaScript Array class is a global object that is used in the construction of arrays. Unlike in Java, JavaScript arrays are not fixed in length or the type they hold.

```javascript
let movies = ["Good Will Hunting", "Interstellar", "The Shawshank Redemption"];

for(let movie in movies){
    console.log(movie);
}
```
```
> 0
> 1
> 2
```

```javascript
for(let movie of movies){
    console.log(movie);
}
```

```
> "Good Will Hunting"
> "Interstellar"
> "The Shawshank Redemption"
```


# JavaScript in the Browser

- provides functionality to our webpage
- include js in our webpages in internal script or external script
  - `<script> //js code goes here </script>`
  - `<script src="my-external-script.js"></script>` (this is easier to test, maintain, and reuse)

```javascript
let myVar = "Hello World":

```

# DOM Manipulation

- Document Object Model, a tree-like object representation of our html elements
- provides us a way to interface with our html using javascript

window

- document
  - doctype declaration
  - html
    - head
    - body ...

### Accessing Elements on Our Webpage using JS

The document object has methods which allow us to target specific nodes in the DOM. These DOM methods allow for programmatic access to the tree of nodes.

- getElementById
- getElementsByClassName
- getElementsByName
- getElementsByTagName

We can access or manipulate attributes of our nodes.

- [element].setAttribute("attribute","value")
- [element].[attribute]
  - [element].innerText
  - [element].src
- [element].hasAttribute
- [element].classList

Access Related Nodes

- [element].firstChild
- [element].lastChild
- [element].nextSibling
- [element].previousSibling
- [element].children

Add/Remove elements to the DOM

- document.createElement("tagname")
- [element].removeChild([element])

Events

- types of interactions with our HTML page that we can handle programmatically with JS

ex.

- onchange
- onclick
- onmouseover
- onkeydown
- onload

1. assign the appropriate attribute of the html tag to a function
   - `<button onclick="submitForm()">Click to submit</button>`
2. set the appropriate property of the javascript object to a function
   - `<button id="submit-button">Click to submit</button>`
   - document.getElementById("submit-button").onClick = function(){//... submits form}
3. create an event listener on the element specifying a callback function \*
   - `<button id="submit-button">Click to submit</button>`
   - document.getElementById("submit-button").addEventListener("click", callback, bubbling/capturing)

# AJAX - Asynchronous JavaScript and XML

- programming practice of building more complex and dynamic webpages which prepare and send HTTP requests
- browser provides an XMLHttpRequest object in order to perform HTTP requests

XMLHttpRequest - readyState

- 0 unsent : object created, open method has not yet been invoked
- 1 opened
- 2 headers_received
- 3 loading
- 4 done

```javascript
const xhr = new XMLHttpRequest();
xhr.open("GET", "https://pokeapi.co/api/v2/pokemon/1");
xhr.onreadystatechange = function () {
  if (readyState === 4) {
    // process response
  }
};
xhr.send();
```
