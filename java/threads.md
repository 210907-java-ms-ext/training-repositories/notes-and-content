# Threads

A thread is a path of execution in your program. It represents a subprocess within your application. The first preexisting thread in your application is that which is running your main method. Additional threads can be started from the main thread. There are also daemon threads, such as the thread responsible for garbage collection, which run in the background. The JVM will execute until all non-daemon threads have finished executing.

Threads are defined by implementing the run method in the Runnable interface. This can be done in 2 ways: (1) by extending the Thread class and overriding the run method or (2) by implementing the Runnable interface and providing the implemented class to an instance of the Thread class. An example of each can be found [here](https://docs.oracle.com/javase/tutorial/essential/concurrency/runthread.html). 

Once the run method is implemented, the start method is called, which creates the subprocess and calls the run method.

- `java.lang.Thread` is the Thread class representing a thread, which you can extend and then override its run() method. Afterwards, you call start().
- `java.lang.Runnable` is a functional interface (meaning only one method) which you can implement and then override run(). Afterwards, you can pass the object to a Thread instance and run start().
- The `synchronized` keyword is a modifier that can be used to write statements or methodsto protect them in a multithreaded environment. When a method is synchronized, for example, only one thread is able to access that block of code at a time. Every object in java has what's called an intrinsic lock (also referred to monitor lock, or just monitor).  When a thread is executing a synchronized block, it needs to acquire the intrinsic lock of the method's calling object, and is said to own the lock for the duration of the execution.  When the thread has completed the execution of the block of code, the intrinsic lock is released. This assures exclusive access to an object's state and a "happens-before" relationship between multiple executions.
- `wait()` and `notify()` or `notifyAll()` methods of `java.lang.Object` can be used to suspend and resume threads.

The [ExecutorService](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) also allows for the creation and management of thread pools. When working with a web server, a thread pool will be allocated (generally 200 threads) for handling HTTP requests.

### MultiThreading

An application which uses multiple non-daemon threads is said to be multithreaded. Mulithreading is a great feature of Java and allows for operations to be performed more efficiently. However, issues can arise when multiple threads are negotiating shared resources. Because of this, multithreading applications must utilize these resources in a thread safe way. One way of doing this is by synchronizing methods or blocks of code in your application - this only allows one thread to access that portion of code at a time.

Issues like [deadlock](https://docs.oracle.com/javase/tutorial/essential/concurrency/deadlock.html) and [starvation](https://docs.oracle.com/javase/tutorial/essential/concurrency/starvelive.html) can also threaten an application's liveness, or an applications ability to execute in a timely manner.

Threads can also be joined, where the current thread will wait until the completion of the joined thread to proceed its execution. This can be visualized [here](https://i.stack.imgur.com/Dfg6w.png).

### Thread States

A thread can be in the following states:

- NEW - A thread that has not yet started
- RUNNABLE - A thread executing in the Java virtual machine
- BLOCKED - A thread that is blocked waiting for a monitor lock. This occurs when a thread is waiting for another thread to leave a synchronized method or block.
- WAITING - A thread that is waiting indefinitely for another thread to perform a particular action is in this state. This happens when the wait method is called.
- TIMED_WAITING - A thread that is waiting for another thread to perform an action for up to a specified waiting time is in this state. This happens when the wait method is called with a specific amount of time.
- TERMINATED - A thread that has exited. Either it has finished executing or it exited exceptionally.

### Producer-Consumer Problem 

The producer-consumer problem (or bounded-buffer problem) occurs when we have multiple threads, one producing data and the other consuming it. They share a common buffer - the producer puts its data in the buffer, and the consumer takes it. The problem here arises when the producer tries to produce data when the buffer is already full, or when the consumer tries to take data from the buffer when it is empty.

There are several solutions to this problem. You can set up your threads to wait if they are unable to perform their desired action, using the wait() and notify() methods. Another popular approach is to use a BlockingQueue. You can see an example of this solution [here](https://howtodoinjava.com/java/multi-threading/producer-consumer-problem-using-blockingqueue/).

![producer/consumer problem](https://howtodoinjava.com/wp-content/uploads/2016/04/blocking-queue.png)

> image from howtodoinjava.com