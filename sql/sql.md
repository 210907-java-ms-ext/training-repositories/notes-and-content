# SQL 
- SQL = Structured Query Language
- a language which provides a means for interacting with a database 

### Relational Databases

- table based architecture, with columns of the tables relating to one another (FK relationships)
- when you define a table, you define the data type of the column, but you can also place additional restrictions on the data using constraints (fk, pk, unique, not null, check)
- data integrity - changes to db follow specifications of the db (types, constraints)
- referential integrity - changes to db maintain relationships specified in our db (can't delete records being referenced, cannot reference an entity or record that doesn't exist, etc)

- Primary Key
    - the column or combination of columns which acts as the unique identifier for each record
    - must be unique
    - cannot be null
    - the primary key can be defined as naturally occurring data (e.g. SSN, email) - this is called a natural key
    - the primary key can also be defined using a more arbitrary value, created solely to identify the record in the database - this is called a surrogate key

# SQL and its sub-languages
- there are many different operations we can perform on our db
- these operations can be broken down into types of operations or sub-languags within SQL

### DDL - Data Definition Language
- used to define the structure of our database entities
- any table scoped operations fall under DDL
- `create`, `drop`, `truncate`, `alter`
- note: when defining tables, we specify the [data type](https://www.postgresql.org/docs/9.5/datatype.html) held in each column, as well as defining any [constraints](https://www.postgresql.org/docs/9.4/ddl-constraints.html) we would like placed - constraints can be placed on a single column, or on a table using multiple columns

### DML - Data Manipulation Language 
- represents the operations we do on the records themselves
- CRUD operations (Create Read Update Delete); the main operations we do on data
- `insert`, `update`, `delete`, `select`
    - `select` is sometimes considered part of its own sublanguage, DQL, or Data Query Language

### TCL - Transaction Control Language
**Transaction:** a unit of work done on a database - it consists of one or more operation
- TCL helps facilitate creating explicit transactions to group together related operations
- `commit`, `rollback`, `savepoint`

### DCL - Data Control Language
- allows you to manage user permissions on a database 
- `grant` `revoke`
