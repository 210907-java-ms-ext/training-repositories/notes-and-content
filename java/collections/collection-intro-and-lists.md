# Java Collections Framework


![collection framework hierarchy](./collections-hierarchy.png)

### Collection

- "Collection" is an interface in which most java data structures inherit from
- Collection provides:
  - size()
  - add(Object o)
  - containsAll(Collection<? extends E> c)
  - remove(Object o)
  - isEmpty()
  - contains(Object o)
  - containsAll(Collection<?> c)
  - iterator()
- Collection is actually a sub-interface of Iterable, so it provides its implementing classes with the methods from Iterable as well
  - forEach(Consumer action)
  - iterator() returns an Iterator (also an interface)
    - Iterator hasNext(), next(), and remove() to iterate over and manipulate Collections
    - good for removing elements and for moving over two collections simultaneously
- the iterable interface is what allows us to use a for-each on a collection
  - any object that implements iterable will allow us to use a dynamic for loop to iterate through it
  - array are an exception to this, as we can use a for each loop with arrays despite them not implementing the iterable interface
- inheriting from Collection we have sub-interfaces: Set, List, and Queue

### List

- contains additional methods for index based operations
  - positional access using methods such as get, set, add, addAll, and remove
  - positional searching methods such as indexOf and lastIndexOf
- can contain duplicate elements

### ArrayList

- linear, array backed data structure
  <img src="https://beginnersbook.com/wp-content/uploads/2013/12/Adding_Element_ArrayList_diagram.png" alt="inserting into arraylist">
  <img src="https://beginnersbook.com/wp-content/uploads/2013/12/Removing_Element_from_ArrayList_diagram.png" alt="deleting from arraylist">

### LinkedList

- linear data structure using a node based architecture
- element data is stored in a node which has a reference pointing to the next node, and also to the previous node if its a doubly linked list
  <img src="https://www.java2novice.com/images/ddlinsert.jpg" alt="insert into a doubly linked list">
  <img src="https://www.java2novice.com/images/dlldelete.jpg" alt="remove from a doubly linked list">