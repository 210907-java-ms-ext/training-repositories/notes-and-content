# SQL Notes Continued

## Functions

**Aggregate functions** - performed on a data set and gives us one aggregated value

- sum, min, max, avg, count, [etc.](https://www.postgresql.org/docs/9.2/functions-aggregate.html)

**Scalar functions** - set number of arguments that return one value

- length(str), current_date(), concat(str1, str2), round(num, n), sqrt(num), pi(), lower(str),[etc.](https://www.postgresql.org/docs/9.2/functions.html)

## Query clauses

select [columns]

from [table]

where [condition] -- filters pre aggregation if aggregation is occurring

group by [column] -- defines how our data is going to be aggregated

having [condition] -- filters after aggregation

order by [column][asc/desc]

## Views
There are two types of views in Postgres which allow us to view a dataset from a query. A traditional *view* will store a query in memory. Each time we make a request to that view, the query is executed and the result set is retrieved from the database.  There is also such thing as a *materialized view.* This type of view stores the actual dataset in memory. Each time we make a request to a materialized view, it does not execute the query again, but rather returns the saved dataset. To update the dataset, the materialized view must be refreshed.

```sql
create [materialized] view as [query]
```

## Transactions

- unit of work on a db, one or more related operations executed as a unit

#### Properties of a Transaction

**Atomicity**

- transactions executes in its entirety or not at all

**Consistency**

- db is in a consistent state before and after a transaction executes
- transaction maintains data and referential integrity

**Isolation**

- concurrent transactions should not affect the execution of one another

**Durable**

- committed data is permanent and cannot be rolled back

#### Transaction Isolation

- [transaction isolation](https://www.postgresql.org/docs/9.5/transaction-iso.html)

**Transaction phenomena**

1. Dirty Read
    - a) begins a transaction
    - a) inserts new user: id=5, name=Paul (uncommitted data)
    - b) queries user table (reads uncommitted data)
    - a) rolls back


- transaction b read uncommitted data

2. Non Repeatable Read
    - a) begins a transaction
    - a) queries user table
    - b) begins a transaction
    - b) updates user 7: changes email from joeshmo@gmail.com to josephshmo@gmail.com
    - b) commits transaction
    - a) (still in the same transaction) queries user table

- transaction a sees inconsistent result sets, records from the queries have fields which have changed

3. Phantom Read
    - a) begins a transaction
    - a) queries user table
    - b) begins a transaction
    - b) remove user 12
    - b) commits transaction
    - a) (still in the same transaction) queries user table, result set has one less record than first query

- transaction a sees an inconsistent result set, records have been added or removed in the time a has been executing

## Normalization

- the process of reducing redundancy and preventing data anomalies

1. First Normal Form

- table has a primary key
- columns hold atomic values
- no repeated columns

2. Second Normal Form

- removed partial dependencies

3. Third Normal Form

- removed transitive dependencies

## Example

<img src="./invoice.png" alt="invoice data">

**invoice**

| invoice_id | customer_name | date        | item       | price | quantity | line price | total |
| ---------- | ------------- | ----------- | ---------- | ----- | -------- | ---------- | ----- |
| 201        | Peter         | 2 June 2020 | Coffee     | 5.00  | 2        | 10.00      | 25.00 |
| 201        | Peter         | 2 June 2020 | Milk       | 3.00  | 1        | 3.00       | 25.00 |
| 201        | Peter         | 2 June 2020 | Bread      | 4.00  | 3        | 12.00      | 25.00 |
| 204        | Patrick       | 3 June 2020 | Cereal     | 3.50  | 3        | 10.50      | 29.25 |
| 204        | Patrick       | 3 June 2020 | Wine       | 7.00  | 2        | 14.00      | 29.25 |
| 204        | Patrick       | 3 June 2020 | Watermelon | 4.75  | 1        | 4.75       | 29.25 |

---

**invoice**

| _invoice_id_ | customer_name | date        | _line_number_ | item       | price | quantity | line price | total |
| ------------ | ------------- | ----------- | ------------- | ---------- | ----- | -------- | ---------- | ----- |
| 201          | Peter         | 2 June 2020 | 1             | Coffee     | 5.00  | 2        | 10.00      | 25.00 |
| 201          | Peter         | 2 June 2020 | 2             | Milk       | 3.00  | 1        | 3.00       | 25.00 |
| 201          | Peter         | 2 June 2020 | 3             | Bread      | 4.00  | 3        | 12.00      | 25.00 |
| 204          | Patrick       | 3 June 2020 | 1             | Cereal     | 3.50  | 3        | 10.50      | 29.25 |
| 204          | Patrick       | 3 June 2020 | 2             | Wine       | 7.00  | 2        | 14.00      | 29.25 |
| 204          | Patrick       | 3 June 2020 | 3             | Watermelon | 4.75  | 1        | 4.75       | 29.25 |

---

invoice_line

| invoice_id | line_number | item       | price | quantity | line price |
| ---------- | ----------- | ---------- | ----- | -------- | ---------- |
| 201        | 1           | Coffee     | 5.00  | 2        | 10.00      |
| 201        | 2           | Milk       | 3.00  | 1        | 3.00       |
| 201        | 3           | Bread      | 4.00  | 3        | 12.00      |
| 204        | 1           | Cereal     | 3.50  | 3        | 10.50      |
| 204        | 2           | Wine       | 7.00  | 2        | 14.00      |
| 204        | 3           | Watermelon | 4.75  | 1        | 4.75       |

invoice

| invoice_id | customer_name | date        | total |
| ---------- | ------------- | ----------- | ----- |
| 201        | Peter         | 2 June 2020 | 25.00 |
| 204        | Patrick       | 3 June 2020 | 29.25 |

---

invoice_line

| invoice_id | line_number | item_no | quantity |
| ---------- | ----------- | ------- | -------- |
| 201        | 1           | 73627   | 2        |
| 201        | 2           | 76283   | 1        |
| 201        | 3           | 72463   | 3        |
| 204        | 1           | 84726   | 3        |
| 204        | 2           | 83725   | 2        |
| 204        | 3           | 82617   | 1        |

invoice

| invoice_id | customer_name | date        |
| ---------- | ------------- | ----------- |
| 201        | Peter         | 2 June 2020 |
| 204        | Patrick       | 3 June 2020 |

item

| item_no | name       | price |
| ------- | ---------- | ----- |
| 73627   | Coffee     | 5.00  |
| 76283   | Milk       | 3.00  |
| 72463   | Bread      | 4.00  |
| 84726   | Cereal     | 3.50  |
| 83725   | Wine       | 7.00  |
| 82617   | Watermelon | 4.75  | 
